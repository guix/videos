﻿1
00:00:01,000 --> 00:00:05,000
Welcome to the Guix package manager tutorial for asking for help.

2
00:00:05,000 --> 00:00:08,000
In this video you will be shown all the ways in which

3
00:00:08,000 --> 00:00:11,000
you could get in touch with us in case you need help

4
00:00:11,000 --> 00:00:13,500
and which resources are also available for you.

5
00:00:14,000 --> 00:00:16,000
Guix is a very kind and welcoming community.

6
00:00:16,000 --> 00:00:21,000
Whether you are a user, an admin or would like to join us,

7
00:00:21,000 --> 00:00:24,000
we will do our best to answer all your questions,

8
00:00:24,000 --> 00:00:27,000
don't be afraid of getting in touch if you need to.

9
00:00:27,000 --> 00:00:30,000
We even have a Code of Conduct to state this,

10
00:00:30,000 --> 00:00:33,000
and that everybody needs to accomplish.

11
00:00:33,000 --> 00:00:36,000
The idea is to provide a warm, friendly and harassment-free

12
00:00:36,000 --> 00:00:37,500
environment for everyone.

13
00:00:37,500 --> 00:00:39,000
This is our main site.

14
00:00:39,000 --> 00:00:46,000
You can see that our URL is https://gnu.org/s/guix/

15
00:00:46,900 --> 00:00:50,000
You have our introductory videos about our documentation

16
00:00:50,000 --> 00:00:54,000
to help you start learning about some topics,

17
00:00:54,000 --> 00:00:55,000
and of course our full documentation 

18
00:00:55,000 --> 00:00:58,000
that will explain in detail everything you need to know.

19
00:00:58,500 --> 00:01:01,000
By the end of our site, you will find this section,

20
00:01:01,000 --> 00:01:05,000
that states the three main ways of getting in touch

21
00:01:10,000 --> 00:01:12,000
other humans when you need help:

22
00:01:12,000 --> 00:01:15,000
our IRC channel, our help mailing list

23
00:01:15,000 --> 00:01:17,000
and our info mailing list.

23
00:01:17,000 --> 00:01:20,000
We will be explaining them soon.

24
00:01:20,000 --> 00:01:23,000
Please never hesitate of getting in touch.

25
00:01:23,000 --> 00:01:26,000
If you click "All contact media", you will

26
00:01:26,000 --> 00:01:29,000
more information about ways of reaching us.

27
00:01:29,000 --> 00:01:32,000
There are two main lists. help-guix and info-guix.

28
00:01:35,000 --> 00:01:38,000
The first one is for asking for help for whatever

28
00:01:38,000 --> 00:01:41,000
you are trying to do, in any of the role

29
00:01:41,000 --> 00:01:44,000
you play: user, admin or contributor. 

30
00:01:44,000 --> 00:01:47,000
The second one is very useful because it provides

31
00:01:47,000 --> 00:01:50,000
information about the state of Guix.

32
00:01:50,000 --> 00:01:53,000
We strongly recommend you to subscribe to it,

33
00:01:53,000 --> 00:01:56,000
so that you know what is happening.

34
00:01:56,000 --> 00:01:59,000
Sometimes you will think something is not working

35
00:01:59,000 --> 00:02:02,000
and it is just that, for instance,

36
00:02:02,000 --> 00:02:05,000
that some substitutes URL's are temporary unavailable.

37
00:02:05,000 --> 00:02:08,000
You will find this kind of information here.

38
00:02:08,000 --> 00:02:11,000
Here you can see how GNU mailman looks like,

39
00:02:11,000 --> 00:02:14,000
that is the software for managing mail discussions,

40
00:02:14,000 --> 00:02:17,000
like help-guix, and newsletters, such us info-guix.

41
00:02:17,000 --> 00:02:20,000
In this case, you can see part of the form

42
00:02:20,000 --> 00:02:23,000
you need to fill for registration for help-guix

43
00:02:23,000 --> 00:02:26,000
You will be asked for your email address, 

44
00:02:26,000 --> 00:02:29,000
an optional name, and a password.

45
00:02:29,000 --> 00:02:32,000
Then you confirm your subscription and that's it.

46
00:02:32,000 --> 00:02:35,000
If you take a deeply look, you may notice that,

47
00:02:32,000 --> 00:02:35,000
behind About subtitle, there is link to Help-Guix-Archives.

48
00:02:35,000 --> 00:02:38,000
If you click there, you can find previous mail

49
00:02:38,000 --> 00:02:41,000
asking for help, and search them too.

50
00:02:41,000 --> 00:02:44,000
As regards info-guix, the mailman is very similar. 

51
00:02:44,000 --> 00:02:47,000
Let's start with our IRC channel. 

52
00:02:47,000 --> 00:02:50,000
Here you can see, again taken from our site,

53
00:02:50,000 --> 00:02:53,000
that it explains that it is the main way

54
00:02:53,000 --> 00:02:58,000
of having a real time answer to any issue you might be facing

55
00:02:53,000 --> 00:02:58,000
You can use the chat widget or an IRC client,

56
00:02:58,000 --> 00:03:01,000
that is up to you. Please, do not feel intimidated

57
00:03:01,000 --> 00:03:03,000
by the number of people online,

58
00:03:03,000 --> 00:03:05,000
we are here to help you.

59
00:03:05,000 --> 00:03:08,000
Even if you don't know very well how to ask for

60
00:03:08,000 --> 00:03:12,000
help, we will ask you for more details.

61
00:03:12,000 --> 00:03:15,000
Everybody has different skills and we are all human,

62
00:03:15,000 --> 00:03:17,000
there are no silly questions. 

63
00:03:17,000 --> 00:03:20,000
Let's start with our IRC channel. 

65
00:03:20,000 --> 00:03:23,000
Here you can see, again taken from our site,
 
66
00:03:23,000 --> 00:03:26,000
that it explains that it is the main way

67
00:03:26,000 --> 00:03:29,00
of having a real time answer to any 

68
00:03:29,000 --> 00:03:31,000
issue you might be facing.

69
00:03:31,000 --> 00:03:34,000
You can use the chat widget or an IRC client,

70
00:03:34,000 --> 00:03:36,000
that is up to you. 

71
00:03:36,000 --> 00:03:39,000
please, do not feel intimidated by

72
00:03:39,000 --> 00:03:42,000
the number of people online,

74
00:03:42,000 --> 00:03:44,000
we are here to help you.

75
00:03:44,000 --> 00:03:46,000
we are here to help you.

76
00:03:46,000 --> 00:03:49,000
Even if you don't know very well

77
00:03:49,000 --> 00:03:51,000
how to ask for help,

78
00:03:51,000 --> 00:03:54,000
we will ask you for more details.

79
00:03:54,000 --> 00:03:57,000
Everybody has different skills and we are all human,

80
00:03:57,000 --> 00:04:00,000
there are no silly questions.

81
00:03:57,000 --> 00:04:00,000
Our IRC channel is hosted in freenode.

82
00:04:00,000 --> 00:04:03,000
Here you have the official documentation about it,

83
00:04:03,000 --> 00:04:06,000
you can explore it if you want to.

84
00:04:00,000 --> 00:04:03,000
Our conversations are logged, but again, please,

85
00:04:03,000 --> 00:04:05,000
don't feel intimidated by that.

86
00:04:05,000 --> 00:04:08,000
Something important is that you have to register to freenode,

87
00:04:05,000 --> 00:04:08,000
and log in everytime you connect to show it is you,

88
00:04:08,000 --> 00:04:11,000
and if you don't use your nickname for a long time,

89
00:04:14,000 --> 00:04:16,000
it tends to expire.

90
00:04:16,000 --> 00:04:19,000
These are freenode policies and they are explained

90
00:04:16,000 --> 00:04:20,000
in their site, even the commands you need to use for working.

91
00:04:20,000 --> 00:04:25,000
in their site, even the commands you need to use for working.







