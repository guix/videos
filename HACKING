-*- mode: org; coding: utf-8; -*-

#+TITLE: Hacking GNU Guix Videos

Copyright © 2019 Laura Lazzati <laura.lazzati.15@gmail.com>

  Copying and distribution of this file, with or without modification,
  are permitted in any medium without royalty provided the copyright
  notice and this notice are preserved.

* Commit Access

All commits that are pushed to the central repository on Savannah must be
signed with an OpenPGP key, and the public key should be uploaded to your user
account on Savannah and to public key servers, such as
‘pool.sks-keyservers.net’.  To configure Git to automatically sign commits,
run:

  git config commit.gpgsign true
  git config user.signingkey CABBA6EA1DC0FF33

You can prevent yourself from accidentally pushing unsigned commits to
Savannah by using the pre-push Git hook called located at ‘etc/git/pre-push’:

  cp etc/git/pre-push .git/hooks/pre-push

When pushing a commit on behalf of somebody else, please add a Signed-off-by
line at the end of the commit log message (e.g. with ‘git am --signoff’).
This improves tracking of who did what.
