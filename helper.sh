#!/bin/sh
# GNU Guix Video --- Scripts for videos presenting Guix
# Copyright © 2019 Gábor Boskovits <boskovits@gmail.com>
# Copyright © 2019 Laura Lazzati <laura.lazzati.15@gmail.com>
# Copyright © 2019 Björn Höfling <bjoern.hoefling@bjoernhoefling.de>
#
# This file is part of GNU Guix.
#
# GNU Guix is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# GNU Guix is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

echo "Setting up fonts..."

cp -r fonts ~/.fonts
fc-cache -f

echo "Starting video builds..."

if [ "$#" -ge 2 ]; then
    exit 1
fi

if [ "$#" -eq 1 ]; then #we are building a single video
    VIDEOS=$1
else
    VIDEOS=$(find . -maxdepth 1\
	      \( -path ./fonts -o -path ./.git -o -path ./common \) \
	      -prune -o -type d  ! -name . -print)
fi

echo "The following videos will be built:"
echo $VIDEOS

for fn in $VIDEOS;do
    if [ ! -d $fn ]; then
	    exit 1
    fi
    cd $fn
    ./buildall.sh
    cd ..
done
exit 0
