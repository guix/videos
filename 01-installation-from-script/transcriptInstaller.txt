-8<---------------cut here---------------start------------->8---
SLIDE 1:(12 sec)

Welcome to the Installation Tutorial for the Guix package manager. In
this video we will show you how to install Guix on top of your
GNU/Linux distribution using the official installation script.
-------------------------------------------------------------------
SLIDE 2: (12 sec)

The installation consists of the following three main steps.  First,
we need to fetch the installation script.  Then, we run the script.
Finally, we test that Guix has been correctly installed.
-------------------------------------------------------------------------
SLIDE 3:(27 sec)

First, you need to become the root user.  Then you need to download
the installation script from our website.  We use wget for that.
Last but not least you need to check that it comes from us and that
files have not been tampered with.  For doing this we use gpg, a tool
for securing digital communication.  Gpg works pretty much like a
handwritten signature on a paper document.  In the Guix case we have
our own public key from the person who created the archive file.
------------------------------------------------------------------------
SLIDE 4: (4 secs)

Let's see all this in action.
-------------------------------------------------------------------------
CLI session 1:

As the root user we are fetching our installation script with wget.
Let's now fetch the author's public key.  The output tells us that the
signature is good so we can move on and run the script.  You will be
shown our logo and the homepage URL and asked to press the Return key
to go on.  As you can see the script first checks your system details.
It downloads the latest Guix release, creates the target directories,
sets up the root user's Guix profile and creates build groups and user
accounts for the Guix daemon.  Then it starts the daemon, makes the
guix command available to all users on the system and stops.

Here it lets you choose whether to download package binaries (called
'substitutes') from the project's build farm.  We are going to accept
by typing 'yes' so that we don't have to build all packages from
source.  Lastly the script shows that everything has been installed
as intended.
----------------------------------------------------------------------!
SLIDE 5: (9 secs)

We have installed Guix with our script but how can we be sure that it
is working properly?  We will run a typical command and check the
output.
---------------------------------------------------------------------
CLI session 2:

The easiest way of doing this is to install a test package named
hello. Note that we are running the command as an unprivileged user.
[guix package -i hello]

The output of running the 'hello' command is correct.  So we can be
confident that the installation process was successful.
-----------------------------------------------------------------
SLIDE 6 (7 sec)

And that's it!  For further information, please refer to the Guix
manual.
-8<---------------cut here---------------start------------->8---
