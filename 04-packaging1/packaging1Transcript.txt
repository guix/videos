-8<---------------cut here---------------start------------->8---
SLIDE 1:(11 sec)

Welcome to the Packaging Tutorial for the Guix package manager. This
is the first video in a series of three.  In this tutorial we will
show you how to set up your environment for creating a new package.
-------------------------------------------------------------------
SLIDE 2: (27 sec)

For setting up your environment you need to have a working
installation of Guix plus the source code.  You can download the
latest version of the source code from our git repository.  After that
you need to set up the development environment for Guix.  For
convenience this can be done using the 'guix environment' command.
Then you need to create the system infrastructure, by bootstrapping.
Then come the 'configure' and 'make' stages.  The new guix that is
produced is ready to use without the need to be installed first.
-------------------------------------------------------------------
SLIDE 3: (5 sec)

Let's see all this in a console so that it is easier to understand.
---------------------------------------------------------------------
CLI 1: (47 secs)

In order to get the latest version of Guix, you need to git clone our
repository and change to the new directory.

For the next step, which is setting up our development environment, we
can use our existing installation of Guix!  This means that we are
starting a new shell where all the dependencies and environment
variables are set up.

We add the '--pure' option to unset all the environment variables we
might have in the parent shell and '--ad-hoc' for using extra packages
and tools.

'./bootstrap' is the command to prepare our system.  We configure our
system as usual with './configure', passing '/var' to the
'--localstatedir' option.

Notice that we are not calling 'make install', just 'make'.

Also, we will be prefixing the new guix commands by 'pre-inst-env'.
'pre-inst-env' sets all the necessary environment variables for us.

After make finishes we can see how this works.  First we check where
guix is and we can see that it is not installed.  Now, with
'pre-inst-env', we ask for 'guix size --help' and we can see that
there is output shown.
------------------------------------------------------------------
SLIDE 4: (7 sec)

So that will be the packaging environment that we will use for
creating our new package in the next video.
------------------------------------------------------------------
SLIDE 5: (5 sec)

And that's is.  For further information please refer to the Guix
manual.
-8<---------------cut here---------------end------------->8---





 

