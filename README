-*- mode: org -*-

* Introduction

In order to create the videos, you should run:

$ build-video.sh

The script’s usage is very simple. It has two modes. If no arguments are
supplied, all the videos are created, based on the name of the existing
subdirectories. Nevertheless, if the name of one of the directories is passed as
an argument, only the video it includes is created. This script sets
up an environment with the packages needed and calls a helper script
that spawns the build of each video.  The execution might take a long
time, so please, don't cancel it unless you want to you will find the
explanation for each step inside these scripts.

If you want to delete all the videos, you can delete them by running:

$ make VIDEO=<video-name> clean

and thus have your directory clean.

Each final  video will be composed of slide videos showing concepts  and might
have videos with command line prompts in between - a.k.a CLI videos -.  All of
these videos will then be glued together into the final video for a certain
topic. We are including here an example video, so that you can understand the
process.

This README file includes an example of how the process of creating a video
works.

Videos accept being translated to other languages.

* License

All source code is licensed under the GNU GPL v3 or later. See COPING for
details.

Media files (including transcripts, audio files, SVG slides, translations
and final videos) are licensed under Creative Commons
Attribution-ShareAlike 4.0 International. See the file cc-by-sa.txt for the
full license.

* Tree

The tree structure is described as follows, and, in case new videos are added, 
the structure of it’s subdirectory must be manually created. 

/
|-README
|-COPYING
|-CREDITS
|-Makefile
|-build-video.sh
|-create-cli-video.sh
|-helper.sh
|-screen
|-fonts/
|-common/
| |-audio/
| |-svg/ 
|-<videoName>
  |-slidesList.mk
  |-buildall.sh
  |-transcript<videoName>.txt
  |-en_US
  | |-audios/
  | |-svgs/
  | |-slides/
  | |-subtitles/
  | |-pots/ #not translated
  | |-translationFiles/ #.mo and .po  *ONLY* for this locale
  | |-out/
  | |-videos/
  |-sessions/ 
  |-<localeName> #if translated
    |-audios/
    |-svgs
    |-pots/ #translated
    |-slides/
    |-subtitles/
    |-out/
    |-videos/
    |-sessions/

* Considerations for Creating new Videos
 
** Running make for Creating a Video that Shows CLI Commands and Their Output

$ ./create-cli-video.sh  <videoDirectory> <locale> <sessionFilename> <numberOfCLIVideo>

where:
videoDirectory=name of the video’s directory,
locale=locale for which the video is being created. Default is
en_US,
sessionFilename=file that contains commands and/or metacommands,
numberOfCLIVideo=number that indicates the order in which the video will be
shown I.e. if numberOfCLIVideo=2 it is the second CLI video that will be included
in the final video.

Example: 
$ ./create-cli-video.sh 01-installation-from-script en_US firstCli 1

** Running make for Creating a Video that Shows Slides

$ make videoNoCli VIDEO=<videoName> LOCALE_LANG=<locale>
 NUMBER=<numberOfNoCLIVideo>

Example:
$ make videoNoCli VIDEO=example LOCALE_LANG=en_US NUMBER=1

Important: The name of the audio file and slide number must be the exactly
the same. For instance, if our video is composed by 4.svg, 5.svg and 6.svg, we
will need 4.mp3 5.mp3 6.mp3 files even if our NUMBER argument is 2, since that
means the number of noCLIVideo we are creating, not the files that compose it.


** Running make for Creating Final Video

$ make finalvideo VIDEO=<video> LOCALE_LANG=<locale>


* Running make for Translations

$ make pots_for_video NUMBER=<numberOfNoCliVideo>

Creates the .pots only under en_US locale subdir for the number of slide video
specified to send them to translators.

Example:
$ make pots_for_video NUMBER=2

$ make slides VIDEO=<videoName> LOCALE_LANG=<locale>

Creates the translated slides, once the .pot files return from translators, so
that they can be used for locales other than en_US. 

The locale subdir, at least <locale>/slides must be created
beforehand.  

* Cleaning the Working Directory

$ make clean VIDEO=<video> LOCALE_LANG=<locale>

Cleans all the files related to video creation and translation, by calling all
the other clean_* options.  If VIDEO is not provided, it cleans the DEFAULT one.
If LOCALE_LANG is not provided it cleans en_US.

$ make clean_noCli VIDEO=<video> LOCALE_LANG=<locale>

Cleans all the slide videos.

$ make clean_cli VIDEO=<video> LOCALE_LANG=<locale>

Cleans all the videos that do show CLI commands. 

$ make clean_intermediate_videos VIDEO=<video> LOCALE_LANG=<locale>

Cleans all the intermediate videos, both slides and cli sessions. 

$ make clean_finalVideo VIDEO=<video> LOCALE_LANG=<locale>

Cleans the final video, that glues all the intermediate videos, but not the
intermediate ones. 

$ make clean_pots VIDEO=<video>

Cleans the .pot files to send to translators for the given video.
If VIDEO is not provided it uses the default one.

$ make clean_locale_slides VIDEO=<video> LOCALE_LANG=<LOCALE>

Cleans the translation files "*.po" and "*.mo" inside the en_US subdir for the
video.  If VIDEO is not provided it uses the default one. However,
LOCALE_LANG is *MANDATORY*.  If it is not given make aborts.
