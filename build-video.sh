#!/bin/sh
# GNU Guix Video --- Scripts for videos presenting Guix
# Copyright © 2019 Gábor Boskovits <boskovits@gmail.com>
# Copyright © 2019 Laura Lazzati <laura.lazzati.15@gmail.com>
#
# This file is part of GNU Guix.
#
# GNU Guix is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# GNU Guix is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

# Script to enter a Guix container which has all
# necessary packages to build the videos plus some
# convenient ones.

PACKAGES="paps make imagemagick guile ffmpeg inkscape coreutils findutils cairo-xcb"

echo "Entering a Guix container to build the videos."
echo "Available packages in the container: $PACKAGES"
guix environment -C --ad-hoc $PACKAGES -- ./helper.sh $1

