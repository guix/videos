-8<---------------cut here---------------start------------->8---
SLIDE 1: (11 secs)

Welcome to the Daily Use Tutorial for the Guix package manager. In
this video we will explain the main concepts that characterise GNU
Guix and the main commands to use.
-------------------------------------------------------------------
SLIDE 2: (30 secs)

'guix package' is the command that allows users to install, upgrade
and remove packages.  There is no need to know how to build them or to
have root permission.  Each package goes to the 'store' and each user
has their own profile pointing to their specific packages.  The
profile belongs only to them and does not interfere with other users.
Each time a guix package operation is completed a new generation of
the profile is created.
-------------------------------------------------------------------------
SLIDE 3 (4 secs)

Let's start with some commands and see how this works.
-------------------------------------------------------------------------
CLI 1

(5 secs)

First, we are going to see how to install our example package, hello.
------

(12 sec)

The output is telling us that we have two packages in our profile.
Let's see which packages these are.
------

(10 secs)

Now, let's install another package called 'recutils' that is very
useful, since it lets us search for packages in a more refined way.
----

(5 secs)

Here it shows us that we now have three packages in our profile.
----

(5 sec)

We know that the hello package is our "hello world" package, so let's
remove it.
-----

(5 sec)

We mentioned that there was a term named "generation".  Let's inspect
the generations that we have.
-----

(26 secs)

We can see that our profile has four generations.  In the first
generation we have our locales, that belong to the configuration of
our system.  The second generation shows us the installation of the
hello package.  We can recognise this by the plus sign next to the
package name.  Then we installed recutils so we have our third
generation with that package added.  Our last and current generation
is number four.  That shows that we have removed the hello package.
----

(24 sec)

Another detail of the output that it is worth mentioning is that each
package shows its name and then the word 'out'.  This means that
either the package has only one output or that we have installed the
default one.  Why would some packages be split into more than one
part?  One reason is to save disk space when we don't need all parts
of the package.  Sometimes the Graphical User Interface is not needed,
for example.  In such cases a different word may identify the specific
output that is installed.  In some cases you may have more than one
entry in the list apart from the default one.
------------------------------------------------------------------------
SLIDE 4: (18 secs)

Every time you call 'guix package' you are doing an 'atomic'
transaction.  That is to say the transaction is either completed or
the system is rolled back to the previous state.  When transactions
are successful the system can be rolled back manually, if required.
This makes GNU Guix very powerful.  Let's see some examples.
-------------------------------------------------------------------------
CLI session 2: (46 secs)

First, let's check to see what we have installed.  Now, we will
install emacs and cancel it in the middle of the transaction.  We
check our packages again... and we can see emacs is not there.  Now,
let's install it.  We check our generations and now we roll back.  We
list our generations again.  The current one is number 4 instead of
number 5.  If we perform new operations from here generation number 5
will be overwritten.
----------------------------------------------------------------------
SLIDE 7:

And that's it. For further information please refer to the Guix
manual.
-8<---------------cut here---------------start------------->8---
